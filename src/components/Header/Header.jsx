import React, { useState } from 'react'
import './Header.scss'
import Button from '../Button/Button.jsx'
import ModalWindow from '../ModalWindow/ModalWindow.jsx'

const Header = () => {
    const [isModalOpen, setModalOpen] = useState(false);
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [nameError, setNameError] = useState('');
    const [phoneError, setPhoneError] = useState('');

    const openModal = () => {
        setModalOpen(true);
    };
  
    const closeModal = () => {
        setModalOpen(false);
    };

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
        if (!value.trim()) {
            setNameError('Name is required');
        } else {
            setNameError('');
        }
    };
    
    const handlePhoneChange = (event) => {
        const value = event.target.value;
        setPhone(value);
        if (!value.trim()) {
            setPhoneError('Phone is required');
        } else {
            setPhoneError('');
        }
    };
    
    const handleSubmit = () => {
        if (!name.trim()) {
            alert('Name is required');
            return;
        }
    
        if (!phone.trim()) {
            alert('Phone is required');
            return;
        }
    
        if (!/^\d{10,}$/.test(phone)) {
            alert('Invalid phone number');
            return;
        }
    
        console.log('Name:', name);
        console.log('Phone:', phone);
        // API
        
        closeModal()
    }

    return (
        <> 
        <header className='header'>
            <img className='header__logo' src="./src/assets/svg/Logo.svg" alt="Logo" />
            <nav className='header__nav'>
                <ul className="header__list">
                    <li className="header__list_link"><a href="#">HOME</a></li>
                    <li className="header__list_link">BUY</li>
                    <li className="header__list_link"><a href="#">BLOG</a></li>
                    <li className="header__list_link"><a href="#">ABOUT</a></li>
                    <li className="header__list_link"><a href="#">CONTACT</a></li>
                </ul>
            </nav>
                <div className="header__action">
                <Button onClick={openModal} className="header__btn">Book a consultation</Button>
                <div className="header__language">
                    <Button className="header__language_en">EN</Button>
                    <Button className="header__language_ua">UA</Button>
                </div>
                <p className="header__phone">+38(068)603-76-30</p>
                {/* <div class="header__burger">
                    <span></span>
                </div> */}
            </div>
        </header> 
        <ModalWindow isOpen={isModalOpen} onClose={closeModal} content={
            <>
                <h3 className='modal__title'>Leave your contacts</h3>
                <p className='modal__subtitle'>we will contact you within three hours</p>
                <form className='modal__form'>
                    <label htmlFor="name" />
                    <input className='modal__form_input' type="text" id="name" value={name} onChange={handleNameChange} placeholder='Name' />
                    <label htmlFor="phone" />
                    <input className='modal__form_input' type="text" id="phone" value={phone} onChange={handlePhoneChange} placeholder='Phone' />
                    <button className='modal__form_button' type="button" onClick={handleSubmit}>Submit</button>
                </form>
            </>
        } 
        />
        </>
    )
}

export default Header