import './Baner.scss';

const Baner = () => {

    return (
        <>
        <section className='baner'>
            <div className="baner__content">
                <p className='baner__subtitle'>Lorem Ipsum</p>
                <h1 className='baner__title'>Welcome Home<br /> <span className='baner__title_span'>To</span> Luxury</h1>
                <button className='baner__action'>Book a consultation</button>
            </div>
            <div className="baner__socials">
                <a href="#" className="baner__socials_link" target="_blank" rel="noopener noreferrer">
                    <img className='baner__socials_img' src="./src/assets/svg/facebook.svg" alt="Facebook" />
                </a>
                <a href="#" className="baner__socials_link" target="_blank" rel="noopener noreferrer">
                    <img className='baner__socials_img' src="./src/assets/svg/twitter.svg" alt="Twitter" />
                </a>
                <a href="#" className="baner__socials_link" target="_blank" rel="noopener noreferrer">
                    <img className='baner__socials_img' src="./src/assets/svg/youtube.svg" alt="YouTube" />
                </a>
                <a href="#" className="baner__socials_link" target="_blank" rel="noopener noreferrer">
                    <img className='baner__socials_img' src="./src/assets/svg/instagram.svg" alt="Instagram" />
                </a>
                <a href="#" className="baner__socials_link" target="_blank" rel="noopener noreferrer">
                    <img className='baner__socials_img' src="./src/assets/svg/linkedin.svg" alt="LinkedIN" />
                </a>
            </div>
            <div className="baner__list">
                <div className="baner__list_point">
                    <p className="baner__list_number active">01</p>
                    <p className="baner__list_text">Lorem Ipsum<br />Dolorem apset</p>
                </div>
                <div className="baner__list_point">
                    <p className="baner__list_number">02</p>
                    <p className="baner__list_text">Lorem Ipsum<br />Dolorem apset</p>
                </div>
                <div className="baner__list_point">
                    <p className="baner__list_number">03</p>
                    <p className="baner__list_text">Lorem Ipsum<br />Dolorem apset</p>
                </div>
            </div>
        </section>
        </>
    )
};

export default Baner;
