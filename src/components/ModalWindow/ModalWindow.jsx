import './ModalWindow.scss'

const ModalWindow = ({ isOpen, onClose, content}) => {
    if (!isOpen) return null

    return(
        <div className="modal__overlay">
            <div className="modal">
                <button className='modal__close' onClick={onClose}></button>
                {content}
            </div>
        </div>
    )
}

export default ModalWindow