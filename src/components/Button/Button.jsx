import './Button.scss'

const Button = (props) => {
    const {type = "button", classNames, children, click, ...restProps} = props;

    return (
        <>
            <button onClick={click} type={type} {...restProps}>{children}</button>
        </>
    )
}

export default Button