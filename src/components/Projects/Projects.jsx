import React, { useState } from 'react';
import './Projects.scss'

const ProjectItem = ({imageSrc, textAlt, textTitle, textSubtitle, text}) => {
    return (
        <React.Fragment>
            <img src={imageSrc} alt={textAlt} style={{ width: '430px', height: '570px' }}/>
            <p className="projects__card_title">{textTitle}</p>
            <div className="projects__card_hover">
                <p className="projects__card_subtitle">{textSubtitle}</p>
                <p className="projects__card_text">{text}</p>
                <button className='projects__card_action'>See project</button>
            </div>
        </React.Fragment>
    )
}

const Projects = () => {
    const [activeTab, setActiveTab] = useState('all');

    const handleTabClick = (event) => {
      const clickedTabId = event.target.getAttribute('data-id');
  
      if (clickedTabId === 'all') {
        setActiveTab('all');
      } else {
        setActiveTab(clickedTabId);
      }
    };

    return (
        <>
        <section className='projects'>
            <div className="projects__header">
                <h3 className='projects__title'>Latest projects</h3>
                <ul className="projects__list" onClick={handleTabClick}>
                    <li className={`projects__list_filter ${activeTab === 'all' ? 'project__list_filter active' : ''}`} data-id="all">
                        All
                    </li>
                    <li className={`projects__list_filter ${activeTab === 'building' ? 'project__list_filter active' : ''}`} data-id="building">
                        Building
                    </li>
                    <li className={`projects__list_filter ${activeTab === 'interior' ? 'project__list_filter active' : ''}`} data-id="interior">
                        Interior
                    </li>
                </ul>
            </div>
            <ul className="projects__cards">
                <li className={`projects__card ${activeTab !== 'building' && activeTab !== 'all' ? 'hide' : ''}`} data-id="building" width='430px'>
                    <ProjectItem
                        imageSrc = './src/assets/img/villa.jpg'
                        textAlt = 'Villa'
                        textTitle = 'Villas'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
                <li className={`projects__card ${activeTab !== 'building' && activeTab !== 'all' ? 'hide' : ''}`} data-id="building">
                    <ProjectItem
                        imageSrc = './src/assets/img/house.jpg'
                        textAlt = 'House'
                        textTitle = 'Houses'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
                <li className={`projects__card ${activeTab !== 'interior' && activeTab !== 'all' ? 'hide' : ''}`} data-id="interior">
                    <ProjectItem
                        imageSrc = './src/assets/img/villa2.jpg'
                        textAlt = 'Villa'
                        textTitle = 'Villas'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
                <li className={`projects__card ${activeTab !== 'building' && activeTab !== 'all' ? 'hide' : ''}`} data-id="building">
                    <ProjectItem
                        imageSrc = './src/assets/img/house2.jpg'
                        textAlt = 'House'
                        textTitle = 'Houses'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
                <li className={`projects__card ${activeTab !== 'interior' && activeTab !== 'all' ? 'hide' : ''}`} data-id="interior">
                    <ProjectItem
                        imageSrc = './src/assets/img/villa3.jpg'
                        textAlt = 'Villa'
                        textTitle = 'Villas'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
                <li className={`projects__card ${activeTab !== 'interior' && activeTab !== 'all' ? 'hide' : ''}`} data-id="interior">
                    <ProjectItem
                        imageSrc = './src/assets/img/house4.jpg'
                        textAlt = 'House'
                        textTitle = 'Houses'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
                <li className={`projects__card ${activeTab !== 'building' && activeTab !== 'all' ? 'hide' : ''}`} data-id="building">
                    <ProjectItem
                        imageSrc = './src/assets/img/house3.jpg'
                        textAlt = 'House'
                        textTitle = 'Houses'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
                <li className={`projects__card ${activeTab !== 'interior' && activeTab !== 'all' ? 'hide' : ''}`} data-id="interior">
                    <ProjectItem
                        imageSrc = './src/assets/img/house5.jpg'
                        textAlt = 'Villa'
                        textTitle = 'Villas'
                        textSubtitle = 'Dubai'
                        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat molestie integer aliquam consectetur. Faucibus vitae dui massa tellus magna sit.'
                    />
                </li>
            </ul>
        </section>
        </>
    )
}

export default Projects