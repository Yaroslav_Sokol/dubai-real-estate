import Header from './components/Header/Header.jsx'
import Baner from './components/Baner/Baner.jsx'
import Projects from './components/Projects/Projects.jsx'
import './App.scss'

const App = () => {

  return (
    <>
      <Header />
      <Baner />
      <Projects />
    </>
  )
}

export default App 